require('./polyfills')

var core = require('./core');
require('./bosh');
require('./websocket');
module.exports = core;
